#!/usr/bin/env bash

set -e


mkdir -p $PREFIX/data/truenet/models/

for modeldir in $(ls -d *); do
  if [ -d ${modeldir} ]; then
    cp -r ${modeldir} $PREFIX/data/truenet/models/
  fi
done

chmod -R 0755 $PREFIX/data/truenet/models/*
