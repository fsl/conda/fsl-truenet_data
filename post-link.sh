#!/usr/bin/env bash

# Truenet expects data files to be located in
# $FSLDIR/data/truenet/. However, truenet may be
# installed into a different environment, e.g. a
# child environment at $FSLDIR/envs/truenet/.  So
# after installation, we create links from the data
# in $FSLDIR/envs/truenet/data/truenet/ to
# $FSLDIR/truenet/data/,

# Only link data if the FSL_CREATE_WRAPPER_SCRIPTS
# environment variable is set
if [ -z "${FSL_CREATE_WRAPPER_SCRIPTS}" ]; then
  exit 0
fi

# If installing into a child environment (e.g.
# $FSLDIR/envs/truenet/) create hard-links for
# the truenet data files into $FSLDIR/data/truenet
if [[ ${PREFIX} != ${FSLDIR} && ${PREFIX} == ${FSLDIR}* ]]; then
  for modeldir in ${PREFIX}/data/truenet/models/*; do
    if [ ! -d ${modeldir} ]; then
      continue
    fi
    modelname=$(basename $modeldir)
    fslmodeldir=${FSLDIR}/data/truenet/models/${modelname}
    mkdir -p ${fslmodeldir}
    for modelfile in ${modeldir}/*; do
      ln ${modelfile} ${fslmodeldir}/
    done
  done
fi
